import os

#Permet de déterminer si une année est bissextile ou non
def Bissextile(annee):
    bissextile = False
    if annee%400==0:
        bissextile=True
    elif annee%100==0:
        bissextile=False
    elif annee%4==0:
        bissextile=True
    return bissextile
#retourne un chiffre en fonction du mois
def calendrier(Mois):
    if Mois =="janvier" or Mois =="octobre":
        douze = 0
    if Mois =="février" or Mois =="mars" or Mois =="novembre":
        douze = 3
    if Mois =="avril" or Mois =="juillet":
        douze = 6
    if Mois =="mai":
        douze = 1
    if Mois =="juin":
        douze = 4
    if Mois =="aout":
        douze = 2
    if Mois == "septembre" or Mois == "décembre":
        douze = 5
    return douze
#retourne un chiffre en fonction du siècle
def siecle (annee):
    b = int(annee[0:2])
    Siecle = 0
    if b == 16:
        Siecle = 6
    if b == 17:
        Siecle = 4
    if b == 18:
        Siecle = 2
    if b == 19:
        Siecle = 0
    if b == 20:
        Siecle = 6
    if b == 21:
        Siecle = 4
    return Siecle


print("Ce programme va vous aider à déterminer le jour de la semaine d'une date")


Jour = input("saisissez le jour : ")

Mois = input("saisissez le mois (en lettre minuscule) : ")

Annee = input("saisissez enfin L'année : ")

Jour = int(Jour)

Last = int(Annee[2:4])

quart = Last//4
#on récupère le chiffre associé au mois
quatre = calendrier(Mois)
#on vérifie si est bissextile
Vbissextile=Bissextile(int(Annee))
cinq = 0
if Vbissextile == True and (Mois == "janvier" or Mois == "fevrier") :
    cinq = -1

#on récupère le chiffre associé au siècle
six = siecle(Annee)

total = Last+quart+Jour+quatre+cinq+six

s = total % 7

if s % 7 == 0:
   print("dimanche, ", Jour, Mois, Annee)
elif s % 7 == 1:
   print("lundi, ", Jour, Mois, Annee)
elif s % 7 == 2:
   print("mardi, ", Jour, Mois, Annee)
elif s % 7 == 3:
   print("mercredi, ", Jour, Mois, Annee)
elif s % 7 == 4:
   print("jeudi, ", Jour, Mois, Annee)
elif s % 7 == 5:
   print("vendredi, ", Jour, Mois, Annee)
else:
   print("samedi, ", Jour, Mois, Annee)

os.system("pause")